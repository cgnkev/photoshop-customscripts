/**
 *  OpenFromHere 
 *   
 *  Provides a method of opening related images from this folder, picks file by 
 *  stripping off extra information from the filename dynamically. 
 *   
 *  Place in folder where you would like to open from.  
 *   
 *  Call this script from any action and the filepath will be
 *  determined by this script's current location, and the filename
 *  will be determined on the fly using current filename as a basis. 
 *   
 *  @author Joe Richardson
 *  @version  0.10  
 */


/**
 * Find's the location of the current script. 
 *  
 *   Example: var myCurrentLocationFolder = WhoAmI().parent;
 *           
 *   Note:  For Photoshop => CS3 you can use File($.fileName) to replace this function
 *          Niether not work with binary exported scripts in CS3
 *               
 *  @return  File  Current scripts file object
 */
function WhoAmI() {
   var where;
   try {
      var FORCEERROR = FORCERRROR;
   }
   catch( err ) {
      where = File(err.fileName);
   }
   return where;
}

var scriptFolder = new File(WhoAmI()).parent;
/**
 *  Get current Document and manipulate Name       
 *   
 *  /([_-\s].+)?\.[^\.]+$/ 
 *   
 *  ([_-\s].+)? Removes anything from the first _,-,or space to the file extension
 *  \.[^\.]+    Removes the file extension currently on open file.
 *     
 */
var doc = app.activeDocument;
var Name = doc.name.replace(/([^a-zA-Z0-9].+)?\.[^\.]+$/, '');



var relPath = scriptFolder + "/" + Name + ".jpg";
var relFile = new File(relPath);

if(relFile.exists){
    open (relFile);     
    var relDoc = activeDocument;
	if ( relDoc.layers.length == 1 && relDoc.activeLayer.isBackgroundLayer ) {
		relDoc.activeLayer.isBackgroundLayer = false;
		relDoc.activeLayer.name = relDoc.name;
	}

    //Set a variable holding the current preferences, because we will change  
    //them to our own needs  


   var startPreferences = app.preferences.rulerUnits;  
   app.preferences.rulerUnits = Units.PIXELS;  
   
   /**
    * Scale new image using least significant edge
    * Matching the same edge of our previous image will bring this new image
    * into scale with the current one.
    */              
   if (relDoc.height.value > relDoc.width.value) {   
            relDoc.resizeImage(doc.width.value,null,null,ResampleMethod.BICUBIC);
   }else {
            relDoc.resizeImage(null,doc.height.value,null,ResampleMethod.BICUBIC);   
   }
   
   //Determine the largest of the two images.  
   if (doc.height.value * doc.width.value > relDoc.height.value * relDoc.width.value) {
        
    /**    
     * set the parameters to use for the fit, when the current image is larger
     * than the new one we will fit the new one into the constraints of the current
     * image.
     */           
    var fWidth = doc.width;
    var fHeight = doc.height;    

    app.activeDocument = relDoc;
    
    } else {    

    var fWidth = relDoc.width;
    var fHeight = relDoc.height;    

    app.activeDocument = doc;    
    
    }

        
    var id21 = stringIDToTypeID( "3caa3434-cb67-11d1-bc43-0060b0a13dc4" );
    var desc9 = new ActionDescriptor();
    var id22 = charIDToTypeID( "Wdth" );
    var id23 = charIDToTypeID( "#Pxl" );
    desc9.putUnitDouble( id22, id23, fWidth );
    var id24 = charIDToTypeID( "Hght" );
    var id25 = charIDToTypeID( "#Pxl" );
    desc9.putUnitDouble( id24, id25, fHeight );
    
    executeAction( id21, desc9, DialogModes.NO );
  
    app.activeDocument = relDoc;
 

    relDoc.activeLayer.copy(true);    
    relDoc.close(SaveOptions.DONOTSAVECHANGES)

    doc.artLayers.add();
    doc.paste();

    //set the preferences back to what they were   
    //before we adjusted them  
    app.preferences.rulerUnits = startPreferences;  
    //This is the variable we defined earlier  

}else{
    /**    
     * No file found in this directory.     
     *      
     * Default to a blank layer     
     */
    doc.artLayers.add();

}



