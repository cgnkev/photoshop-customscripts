/**
 *  SaveForWebHere 
 *   
 *  Provides a method of saving for web to predefined locations while
 *  also stripping off extra information from the filename dynamically. 
 *   
 *  Place in folder where you would like to save to.  
 *   
 *  Call this script from any action and the filepath will be
 *  determined by this script's current location, and the filename
 *  will be determined on the fly using current filename as a basis. 
 *   
 *  @author Joe Richardson
 *  @version  0.5  
 */


/**
 * Find's the location of the current script. 
 *  
 *   Example: var myCurrentLocationFolder = WhoAmI().parent;
 *           
 *   Note:  For Photoshop => CS3 you can use File($.fileName) to replace this function
 *          Niether not work with binary exported scripts in CS3
 *               
 *  @return  File  Current scripts file object
 */
function WhoAmI() {
   var where;
   try {
      var FORCEERROR = FORCERRROR;
   }
   catch( err ) {
      where = File(err.fileName);
   }
   return where;
}

var scriptFolder = new File(WhoAmI()).parent;
/**
 *  Get current Document and manipulate Name       
 *   
 *  /([_-\s??].+)?\.[^\.]+$/ 
 *   
 *  ([_-\s].+)? Removes anything from the first _,-,or space to the file extension
 *  \.[^\.]+    Removes the file extension currently on open file.
 *     
 */
var doc = app.activeDocument;
var Name = doc.name.replace(/([_-\s].+)?\.[^\.]+$/, '');

var savePath = scriptFolder + "/" + Name + ".jpg";
var saveFile = new File(savePath);

/**
 * Save As PDF
 *  
 *  Uses saveFile to determine location.  
 */ 

// =======================================================
var id9 = charIDToTypeID( "save" );
    var desc4 = new ActionDescriptor();
    var id10 = charIDToTypeID( "As  " );
        var desc5 = new ActionDescriptor();
        var id11 = stringIDToTypeID( "pdfPresetFilename" );
        desc5.putString( id11, "High Quality Print" );
        var id12 = stringIDToTypeID( "pdfPreserveEditing" );
        desc5.putBoolean( id12, false );
        var id13 = stringIDToTypeID( "pdfCompressionType" );
        desc5.putInteger( id13, 7 );
    var id14 = charIDToTypeID( "PhtP" );
    desc4.putObject( id10, id14, desc5 );
    var id15 = charIDToTypeID( "In  " );
    desc4.putPath( id15, saveFile );
executeAction( id9, desc4, DialogModes.NO );    